locals {
  required_actions = {
    configure_otp = {
      alias = "CONFIGURE_TOTP"
      name = "Configure OTP"
      enabled = true
      default_action = false
      priority = 1
    }
    update_password = {
      alias = "UPDATE_PASSWORD"
      name = "Update Password"
      enabled = true
      default_action = true
      priority = 2
    }
    terms_and_conditions = {
      alias = "terms_and_conditions"
      name = "Terms and Conditions"
      enabled = false
      default_action = false
      priority = 3
    }
    update_profile = {
      alias = "UPDATE_PROFILE"
      name = "Update Profile"
      enabled = true
      default_action = true
      priority = 4
    }
    verify_email = {
      alias = "VERIFY_EMAIL"
      name = "Verify Email"
      enabled = true
      default_action = true
      priority = 5
    }
    delete_account = {
      alias = "delete_account"
      name = "Delete Account"
      enabled = false
      default_action = false
      priority = 6
    }
    webauthn_register = {
      alias = "webauthn-register"
      name = "Webauthn Register"
      enabled = false
      default_action = false
      priority = 7
    }
    webauthn_register_passwordless = {
      alias = "webauthn-register-passwordless"
      name = "Webauthn Register Passwordless"
      enabled = false
      default_action = false
      priority = 8
    }
    update_user_locale = {
      alias = "update_user_locale"
      name = "Update User Locale"
      enabled = false
      default_action = false
      priority = 9
    }
    verify_profile = {
      alias = "VERIFY_PROFILE"
      name = "Verify Profile"
      enabled = true
      default_action = true
      priority = 10
    }
  }
}

import {
  id = "apps-dev/CONFIGURE_TOTP"
  to = keycloak_required_action.dev_configure_otp
}

resource "keycloak_required_action" "dev_configure_otp" {
  realm_id = keycloak_realm.apps_dev.id
  alias = local.required_actions.configure_otp.alias
  name = local.required_actions.configure_otp.name
  enabled = local.required_actions.configure_otp.enabled
  default_action = local.required_actions.configure_otp.default_action
  priority = local.required_actions.configure_otp.priority
}

import {
  id = "apps-prod/CONFIGURE_TOTP"
  to = keycloak_required_action.prod_configure_otp
}

resource "keycloak_required_action" "prod_configure_otp" {
  realm_id = keycloak_realm.apps_prod.id
  alias = local.required_actions.configure_otp.alias
  name = local.required_actions.configure_otp.name
  enabled = local.required_actions.configure_otp.enabled
  default_action = local.required_actions.configure_otp.default_action
  priority = local.required_actions.configure_otp.priority
}

import {
  id = "apps-dev/UPDATE_PASSWORD"
  to = keycloak_required_action.dev_update_password
}

resource "keycloak_required_action" "dev_update_password" {
  realm_id = keycloak_realm.apps_dev.id
  alias = local.required_actions.update_password.alias
  name = local.required_actions.update_password.name
  enabled = local.required_actions.update_password.enabled
  default_action = local.required_actions.update_password.default_action
  priority = local.required_actions.update_password.priority
}

import {
  id = "apps-prod/UPDATE_PASSWORD"
  to = keycloak_required_action.prod_update_password
}

resource "keycloak_required_action" "prod_update_password" {
  realm_id = keycloak_realm.apps_prod.id
  alias = local.required_actions.update_password.alias
  name = local.required_actions.update_password.name
  enabled = local.required_actions.update_password.enabled
  default_action = local.required_actions.update_password.default_action
  priority = local.required_actions.update_password.priority
}

import {
  id = "apps-dev/terms_and_conditions"
  to = keycloak_required_action.dev_terms_and_conditions
}

resource "keycloak_required_action" "dev_terms_and_conditions" {
  realm_id = keycloak_realm.apps_dev.id
  alias = local.required_actions.terms_and_conditions.alias
  name = local.required_actions.terms_and_conditions.name
  enabled = local.required_actions.terms_and_conditions.enabled
  default_action = local.required_actions.terms_and_conditions.default_action
  priority = local.required_actions.terms_and_conditions.priority
}

import {
  id = "apps-prod/terms_and_conditions"
  to = keycloak_required_action.prod_terms_and_conditions
}

resource "keycloak_required_action" "prod_terms_and_conditions" {
  realm_id = keycloak_realm.apps_prod.id
  alias = local.required_actions.terms_and_conditions.alias
  name = local.required_actions.terms_and_conditions.name
  enabled = local.required_actions.terms_and_conditions.enabled
  default_action = local.required_actions.terms_and_conditions.default_action
  priority = local.required_actions.terms_and_conditions.priority
}

import {
  id = "apps-dev/UPDATE_PROFILE"
  to = keycloak_required_action.dev_update_profile
}

resource "keycloak_required_action" "dev_update_profile" {
  realm_id = keycloak_realm.apps_dev.id
  alias = local.required_actions.update_profile.alias
  name = local.required_actions.update_profile.name
  enabled = local.required_actions.update_profile.enabled
  default_action = local.required_actions.update_profile.default_action
  priority = local.required_actions.update_profile.priority
}

import {
  id = "apps-prod/UPDATE_PROFILE"
  to = keycloak_required_action.prod_update_profile
}

resource "keycloak_required_action" "prod_update_profile" {
  realm_id = keycloak_realm.apps_prod.id
  alias = local.required_actions.update_profile.alias
  name = local.required_actions.update_profile.name
  enabled = local.required_actions.update_profile.enabled
  default_action = local.required_actions.update_profile.default_action
  priority = local.required_actions.update_profile.priority
}

import {
  id = "apps-dev/VERIFY_EMAIL"
  to = keycloak_required_action.dev_verify_email
}

resource "keycloak_required_action" "dev_verify_email" {
  realm_id = keycloak_realm.apps_dev.id
  alias = local.required_actions.verify_email.alias
  name = local.required_actions.verify_email.name
  enabled = local.required_actions.verify_email.enabled
  default_action = local.required_actions.verify_email.default_action
  priority = local.required_actions.verify_email.priority
}

import {
  id = "apps-prod/VERIFY_EMAIL"
  to = keycloak_required_action.prod_verify_email
}

resource "keycloak_required_action" "prod_verify_email" {
  realm_id = keycloak_realm.apps_prod.id
  alias = local.required_actions.verify_email.alias
  name = local.required_actions.verify_email.name
  enabled = local.required_actions.verify_email.enabled
  default_action = local.required_actions.verify_email.default_action
  priority = local.required_actions.verify_email.priority
}

import {
  to = keycloak_required_action.dev_delete_account
  id = "apps-dev/delete_account"
}

resource "keycloak_required_action" "dev_delete_account" {
  realm_id = keycloak_realm.apps_dev.id
  alias = local.required_actions.delete_account.alias
  name = local.required_actions.delete_account.name
  enabled = local.required_actions.delete_account.enabled
  default_action = local.required_actions.delete_account.default_action
  priority = local.required_actions.delete_account.priority
}

import {
  id = "apps-prod/delete_account"
  to = keycloak_required_action.prod_delete_account
}

resource "keycloak_required_action" "prod_delete_account" {
  realm_id = keycloak_realm.apps_prod.id
  alias = local.required_actions.delete_account.alias
  name = local.required_actions.delete_account.name
  enabled = local.required_actions.delete_account.enabled
  default_action = local.required_actions.delete_account.default_action
  priority = local.required_actions.delete_account.priority
}

import {
  id = "apps-dev/webauthn-register"
  to = keycloak_required_action.dev_webauthn_register
}

resource "keycloak_required_action" "dev_webauthn_register" {
  realm_id = keycloak_realm.apps_dev.id
  alias = local.required_actions.webauthn_register.alias
  name = local.required_actions.webauthn_register.name
  enabled = local.required_actions.webauthn_register.enabled
  default_action = local.required_actions.webauthn_register.default_action
  priority = local.required_actions.webauthn_register.priority
}

import {
  id = "apps-prod/webauthn-register"
  to = keycloak_required_action.prod_webauthn_register
}

resource "keycloak_required_action" "prod_webauthn_register" {
  realm_id = keycloak_realm.apps_prod.id
  alias = local.required_actions.webauthn_register.alias
  name = local.required_actions.webauthn_register.name
  enabled = local.required_actions.webauthn_register.enabled
  default_action = local.required_actions.webauthn_register.default_action
  priority = local.required_actions.webauthn_register.priority
}

import {
  to = keycloak_required_action.dev_webauthn_register_passwordless
  id = "apps-dev/webauthn-register-passwordless"
}

resource "keycloak_required_action" "dev_webauthn_register_passwordless" {
  realm_id = keycloak_realm.apps_dev.id
  alias = local.required_actions.webauthn_register_passwordless.alias
  name = local.required_actions.webauthn_register_passwordless.name
  enabled = local.required_actions.webauthn_register_passwordless.enabled
  default_action = local.required_actions.webauthn_register_passwordless.default_action
  priority = local.required_actions.webauthn_register_passwordless.priority
}

import {
  to = keycloak_required_action.prod_webauthn_register_passwordless
  id = "apps-prod/webauthn-register-passwordless"
}

resource "keycloak_required_action" "prod_webauthn_register_passwordless" {
  realm_id = keycloak_realm.apps_prod.id
  alias = local.required_actions.webauthn_register_passwordless.alias
  name = local.required_actions.webauthn_register_passwordless.name
  enabled = local.required_actions.webauthn_register_passwordless.enabled
  default_action = local.required_actions.webauthn_register_passwordless.default_action
  priority = local.required_actions.webauthn_register_passwordless.priority
}

import {
  to = keycloak_required_action.dev_update_user_locale
  id = "apps-dev/update_user_locale"
}

resource "keycloak_required_action" "dev_update_user_locale" {
  realm_id = keycloak_realm.apps_dev.id
  alias = local.required_actions.update_user_locale.alias
  name = local.required_actions.update_user_locale.name
  enabled = local.required_actions.update_user_locale.enabled
  default_action = local.required_actions.update_user_locale.default_action
  priority = local.required_actions.update_user_locale.priority
}

import {
  to = keycloak_required_action.prod_update_user_locale
  id = "apps-prod/update_user_locale"
}

resource "keycloak_required_action" "prod_update_user_locale" {
  realm_id = keycloak_realm.apps_prod.id
  alias = local.required_actions.update_user_locale.alias
  name = local.required_actions.update_user_locale.name
  enabled = local.required_actions.update_user_locale.enabled
  default_action = local.required_actions.update_user_locale.default_action
  priority = local.required_actions.update_user_locale.priority
}

import {
  to = keycloak_required_action.dev_verify_profile
  id = "apps-dev/VERIFY_PROFILE"
}

resource "keycloak_required_action" "dev_verify_profile" {
  realm_id = keycloak_realm.apps_dev.id
  alias = local.required_actions.verify_profile.alias
  name = local.required_actions.verify_profile.name
  enabled = local.required_actions.verify_profile.enabled
  default_action = local.required_actions.verify_profile.default_action
  priority = local.required_actions.verify_profile.priority
}

import {
  to = keycloak_required_action.prod_verify_profile
  id = "apps-prod/VERIFY_PROFILE"
}

resource "keycloak_required_action" "prod_verify_profile" {
  realm_id = keycloak_realm.apps_prod.id
  alias = local.required_actions.verify_profile.alias
  name = local.required_actions.verify_profile.name
  enabled = local.required_actions.verify_profile.enabled
  default_action = local.required_actions.verify_profile.default_action
  priority = local.required_actions.verify_profile.priority
}