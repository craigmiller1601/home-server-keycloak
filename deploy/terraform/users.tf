import {
  to = keycloak_user.master_admin_user
  id = "master/e68031b4-b2e0-4505-b990-567d8815cbea"
}

resource "keycloak_user" "master_admin_user" {
  realm_id = keycloak_realm.master.id
  username = local.keycloak_admin.username
  enabled = true
  email = local.keycloak_admin.username
  first_name = local.keycloak_admin.first_name
  last_name = local.keycloak_admin.last_name
  email_verified = true
}